
ASSIGNMENT A2
====================================================================
1.	Objective
The objective of this assignment is to allow students to become familiar with the Model View Controller architectural pattern and the Factory Method design pattern.

2.	Application Description
Use Swing/C# API to design and implement an application for the employees of a book store. The application should have two types of users (a regular user represented by the book store employee and an administrator user) which have to provide a username and a password in order to use the application. 
The regular user can perform the following operations:
-	Search books by genre, title, author.
-	Sell books.
The administrator can perform the following operations:
-	CRUD on books (book information: title, author, genre, quantity, price).
-	CRUD on regular users’ information.
-	Generate a report into a txt or xml file with the books out of stock. 

3.	Application Constraints
The information about users, books and selling will be stored in multiple XML files. Use the Model View Controller in designing the application. Use the Factory Method design pattern for generating the reports.

4.	Requirements
-	Create the analysis and design document (see the template).
-	Implement and test the application.