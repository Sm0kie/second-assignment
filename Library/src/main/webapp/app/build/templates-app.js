angular.module('templates-app', ['admin/admin_home.tpl.html', 'admin/reports.tpl.html', 'books/book.tpl.html', 'books/books.tpl.html', 'books/update_books.tpl.html', 'employee/employee.tpl.html', 'employee/update_employee.tpl.html', 'home/books.tpl.html', 'home/home.tpl.html', 'login/login.tpl.html']);

angular.module("admin/admin_home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admin/admin_home.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  ng-controller=\"EmployeeCtrl\">\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"> View Employees</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"searchBooks\">\n" +
    "                        <i class=\"fa fa-book\"> View books</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"generateReport\">\n" +
    "                        <i class=\"fa fa-users\">Generate Report</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Add new employee\n" +
    "    </h1>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>First name:</label>\n" +
    "        <input type=\"text\" ng-model=\"firstName\" class=\"form-control\" required/>\n" +
    "        <label>Last name:</label>\n" +
    "        <input type=\"text\" ng-model=\"lastName\" class=\"form-control\"required/>\n" +
    "        <label>Email:</label>\n" +
    "        <input type=\"email\" ng-model=\"email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\" class=\"form-control\"required/>\n" +
    "        <label>Username:</label>\n" +
    "        <input type=\"text\" ng-model=\"username\" class=\"form-control\"required/>\n" +
    "        <label>Password:</label>\n" +
    "        <input type=\"password\" ng-model=\"password\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Add\" ng-click=\"addEmployee(firstName,lastName,email,username,password)\" class=\"btn btn-primary\"/>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Employees\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"search\"placeholder=\"Employee Info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>ID</th>\n" +
    "        <th>First Name</th>\n" +
    "        <th>LastName</th>\n" +
    "        <th>Email</th>\n" +
    "        <th>Username</th>\n" +
    "        <th>Manage</th>\n" +
    "        <tr ng-repeat=\"employee in employees |filter:search\">\n" +
    "            <td>{{employee.rid}}</td>\n" +
    "            <td>{{employee.firstName}}</td>\n" +
    "            <td>{{employee.lastName}}</td>\n" +
    "            <td>{{employee.email}}</td>\n" +
    "            <td>{{employee.username}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"manageEmployee({employeeId:employee.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Manage\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("admin/reports.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("admin/reports.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  ng-controller=\"ReportCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"> View Employees</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"searchBooks\">\n" +
    "                        <i class=\"fa fa-book\"> View books</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"generateReport\">\n" +
    "                        <i class=\"fa fa-users\">Generate Report</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Generate Report\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>File name:</label>\n" +
    "        <input type=\"text\" ng-model=\"fileName\" min=\"0\" class=\"form-control\" required/>\n" +
    "        <label>Type:</label>\n" +
    "        <form name=\"myForm\" >\n" +
    "            <label>\n" +
    "                <input type=\"radio\" ng-model=\"fileType\" value=\"XML\"required/>\n" +
    "                XML\n" +
    "            </label>\n" +
    "            <label>\n" +
    "                <input type=\"radio\" ng-model=\"fileType\" value=\"TXT\"required/>\n" +
    "                TXT\n" +
    "            </label>\n" +
    "        </form>\n" +
    "\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Generate\" ng-click=\"generateReport({fileName:fileName},{fileType:fileType})\" class=\"btn btn-default\"/>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("books/book.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("books/book.tpl.html",
    "<html ng-app=\"ngBoilerplate.books\"  ng-controller=\"BookManageCtrl\">\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"> View Employees</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"searchBooks\">\n" +
    "                        <i class=\"fa fa-book\"> View books</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"generateReport\">\n" +
    "                        <i class=\"fa fa-users\">Generate Report</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Book\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <p >\n" +
    "            <span class=\"info\">\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{book.rid}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Title: </th>\n" +
    "            <td>{{book.title}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Author: </th>\n" +
    "            <td>{{book.author}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Genre: </th>\n" +
    "            <td>{{book.genre}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Quantity: </th>\n" +
    "            <td>{{book.quantity}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Price: </th>\n" +
    "            <td>{{book.price}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table>\n" +
    "            <th>\n" +
    "                <a ng-click=\"removeBook({bookId:book.rid})\" class=\"btn btn-danger\">\n" +
    "                    Delete\n" +
    "                </a>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <a ui-sref=\"updateBook({bookId:book.rid})\" class=\"btn btn-warning\">\n" +
    "                    Modify\n" +
    "                </a>\n" +
    "            </th>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>\n" +
    "");
}]);

angular.module("books/books.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("books/books.tpl.html",
    "<html ng-app=\"ngBoilerplate.books\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"> View Employees</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"searchBooks\">\n" +
    "                        <i class=\"fa fa-book\"> View books</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"generateReport\">\n" +
    "                        <i class=\"fa fa-users\">Generate Report</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Add Book\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>Title:</label>\n" +
    "        <input type=\"text\" ng-model=\"title\" class=\"form-control\" required/>\n" +
    "        <label>Author:</label>\n" +
    "        <input type=\"text\" ng-model=\"author\" class=\"form-control\"required/>\n" +
    "        <label>Genre:</label>\n" +
    "        <input type=\"text\" ng-model=\"genre\"  class=\"form-control\"required/>\n" +
    "        <label>Quantity:</label>\n" +
    "        <input type=\"number\" ng-model=\"quantity\" min=\"0\" class=\"form-control\"required/>\n" +
    "        <label>Price:</label>\n" +
    "        <input type=\"number\" ng-model=\"price\" step=\"any\" min=\"0\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Add\" ng-click=\"addBook(title,author,genre,quantity,price)\" class=\"btn btn-primary\"/>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Books\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"q\" placeholder=\"book info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>Book Id</th>\n" +
    "        <th>Title</th>\n" +
    "        <th>Author</th>\n" +
    "        <th>Genre</th>\n" +
    "        <th>Quantity available</th>\n" +
    "        <th>Price</th>\n" +
    "        <th>Manage</th>\n" +
    "        <tr ng-repeat=\"book in books|filter:q \">\n" +
    "            <td>{{book.rid}}</td>\n" +
    "            <td>{{book.title}}</td>\n" +
    "            <td>{{book.author}}</td>\n" +
    "            <td>{{book.genre}}</td>\n" +
    "            <td>{{book.quantity}}</td>\n" +
    "            <td>{{book.price}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"manageBook({bookId:book.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Manage\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "\n" +
    "    </table>\n" +
    "</div>\n" +
    "</html>\n" +
    "");
}]);

angular.module("books/update_books.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("books/update_books.tpl.html",
    "<html ng-app=\"ngBoilerplate.account\" >\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"> View Employees</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"searchBooks\">\n" +
    "                        <i class=\"fa fa-book\"> View books</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"generateReport\">\n" +
    "                        <i class=\"fa fa-users\">Generate Report</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<body>\n" +
    "<div class=\"container\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Update Book Information\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>Title:</label>\n" +
    "        <input type=\"text\" ng-model=\"book.title\" class=\"form-control\" required/>\n" +
    "        <label>Author:</label>\n" +
    "        <input type=\"text\" ng-model=\"book.author\" class=\"form-control\"required/>\n" +
    "        <label>Genre:</label>\n" +
    "        <input type=\"text\" ng-model=\"book.genre\"  class=\"form-control\"required/>\n" +
    "        <label>Quantity:</label>\n" +
    "        <input type=\"number\" ng-model=\"book.quantity\" min=\"0\" class=\"form-control\"required/>\n" +
    "        <label>Price:</label>\n" +
    "        <input type=\"number\" ng-model=\"book.price\" step=\"any\" min=\"0\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Update\" ng-click=\"updateBook({title:book.title},{author:book.author},{genre:book.genre},{quantity:book.quantity},{price:book.price})\" class=\"btn btn-primary\"/>\n" +
    "</div>\n" +
    "</body>\n" +
    "</html>");
}]);

angular.module("employee/employee.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("employee/employee.tpl.html",
    "<html ng-app=\"ngBoilerplate.employee\"  ng-controller=\"EmployeeCtrl\">\n" +
    "<style>\n" +
    "span.info {font-size:150%;}\n" +
    "</style>\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"> View Employees</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"searchBooks\">\n" +
    "                        <i class=\"fa fa-book\"> View books</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"generateReport\">\n" +
    "                        <i class=\"fa fa-users\">Generate Report</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Employee\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <p >\n" +
    "            <span class=\"info\">\n" +
    "            <th>ID: </th>\n" +
    "            <td>{{employee.rid}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>First Name: </th>\n" +
    "            <td>{{employee.firstName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "            <span class=\"info\">\n" +
    "            <th>Last Name: </th>\n" +
    "            <td>{{employee.lastName}}</td>\n" +
    "            </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "                <span class=\"info\">\n" +
    "                   <th>Email: </th>\n" +
    "            <td>{{employee.email}}</td>\n" +
    "                </span>\n" +
    "        </p>\n" +
    "        <p>\n" +
    "                <span class=\"info\">\n" +
    "                   <th>Username: </th>\n" +
    "            <td>{{employee.username}}</td>\n" +
    "                </span>\n" +
    "        </p>\n" +
    "    </div>\n" +
    "    <div class=\"row\">\n" +
    "        <table>\n" +
    "            <th>\n" +
    "                <a ng-click=\"removeEmployee({employeeId:employee.rid})\" class=\"btn btn-danger\">\n" +
    "                    Delete\n" +
    "                </a>\n" +
    "            </th>\n" +
    "            <th>\n" +
    "                <a ui-sref=\"updateEmployee({employeeId:employee.rid})\" class=\"btn btn-warning\">\n" +
    "                    Modify\n" +
    "                </a>\n" +
    "            </th>\n" +
    "        </table>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>\n" +
    "");
}]);

angular.module("employee/update_employee.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("employee/update_employee.tpl.html",
    "<html ng-app=\"ngBoilerplate.admin\"  >\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"admin_home\">\n" +
    "                        <i class=\"fa fa-users\"> View Employees</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"searchBooks\">\n" +
    "                        <i class=\"fa fa-book\"> View books</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "                <li>\n" +
    "                    <a href ui-sref=\"generateReport\">\n" +
    "                        <i class=\"fa fa-users\">Generate Report</i>\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"container\"ng-control=\"UpdateEmployeeCtrl\">\n" +
    "    <div class=\"row\">\n" +
    "        <h1 class=\"page-header\">\n" +
    "            Update Employee\n" +
    "        </h1>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\"  >\n" +
    "        <label>First name:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.firstName\" class=\"form-control\" required/>\n" +
    "        <label>Last name:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.lastName\" class=\"form-control\"required/>\n" +
    "        <label>Email:</label>\n" +
    "        <input type=\"email\" ng-model=\"employee.email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$\" class=\"form-control\"required/>\n" +
    "        <label>Username:</label>\n" +
    "        <input type=\"text\" ng-model=\"employee.username\" class=\"form-control\"required/>\n" +
    "        <label>Password:</label>\n" +
    "        <input type=\"password\" ng-model=\"employee.password\" class=\"form-control\"required/>\n" +
    "    </div>\n" +
    "    <input type=\"submit\" value=\"Update\" ng-click=\"updateEmployeeInfo({firstName: employee.firstName},{lastName: employee.lastName},{email: employee.email},{username:employee.username},{password:employee.password})\" class=\"btn btn-default\"/>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("home/books.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/books.tpl.html",
    "<html ng-app=\"ngBoilerplate.home\" ng-controller=\"BookCtrl\" >\n" +
    "<div class=\"container\" >\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <div class=\"container\">\n" +
    "        <div ng-control=\"BookCtrl\">\n" +
    "            <h1 class=\"page-header\">\n" +
    "                Sell book\n" +
    "            </h1>\n" +
    "            <label>Quantity:</label>\n" +
    "            <input type=\"number\" ng-model=\"quantity\" min=\"0\"  class=\"form-control\" required/>\n" +
    "            <input type=\"submit\" value=\"Sell\" ng-click=\"sellBooks({quantity:quantity})\" class=\"btn btn-default\"/>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<html ng-app=\"ngBoilerplate.home\"  ng-controller=\"HomeCtrl\">\n" +
    "<div class=\"container\" ng-show=\"isLoggedIn()\">\n" +
    "\n" +
    "    <div class=\"navbar navbar-default\">\n" +
    "        <div class=\"navbar-header\">\n" +
    "            <div class=\"navbar-brand\">\n" +
    "                <ul class=\"nav navbar-nav\">\n" +
    "                    <div ng-show=\"isLoggedIn()\">\n" +
    "                        <a ng-click=\"logout()\" ui-sref=\"login\">\n" +
    "                            Logout\n" +
    "                        </a>\n" +
    "                    </div>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"collapse navbar-collapse\" collapse=\"menuCollapsed\">\n" +
    "            <ul class=\"nav navbar-nav\">\n" +
    "                <li ui-sref-active=\"active\">\n" +
    "                    <a href ui-sref=\"home\">\n" +
    "                        Home\n" +
    "                    </a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row\">\n" +
    "    <h1 class=\"page-header\">\n" +
    "        Books\n" +
    "    </h1>\n" +
    "    <div class=\"form-group\">\n" +
    "        <input type=\"text\" class=\"form-control\" ng-model=\"q\" placeholder=\"book info\"/>\n" +
    "    </div>\n" +
    "\n" +
    "    <table class=\"table table-striped\">\n" +
    "        <th>Book Id</th>\n" +
    "        <th>Title</th>\n" +
    "        <th>Author</th>\n" +
    "        <th>Genre</th>\n" +
    "        <th>Quantity available</th>\n" +
    "        <th>Price</th>\n" +
    "        <th>Operation</th>\n" +
    "        <tr ng-repeat=\"book in books|filter:q \">\n" +
    "            <td>{{book.rid}}</td>\n" +
    "            <td>{{book.title}}</td>\n" +
    "            <td>{{book.author}}</td>\n" +
    "            <td>{{book.genre}}</td>\n" +
    "            <td>{{book.quantity}}</td>\n" +
    "            <td>{{book.price}}</td>\n" +
    "            <td>\n" +
    "                <a ui-sref=\"sellBook({bookId:book.rid})\" class=\"btn btn-large btn-info\">\n" +
    "                    Sell Book\n" +
    "                </a>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "\n" +
    "    </table>\n" +
    "</div>\n" +
    "</html>");
}]);

angular.module("login/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("login/login.tpl.html",
    "<html ng-app=\"ngBoilerplate.login\" ng-controller=\"LoginCtrl\">\n" +
    "<div class=\"row\">\n" +
    "  <h1 class=\"page-header\">\n" +
    "      Login\n" +
    "  </h1>\n" +
    "  <form ng-submit=\"login()\">\n" +
    "      <div class=\"form-group\">\n" +
    "          <label>Username:</label>\n" +
    "          <input type=\"text\" ng-model=\"user.username\" class=\"form-control\"/>\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "          <label>Password:</label>\n" +
    "          <input type=\"password\" ng-model=\"user.password\" class=\"form-control\"/>\n" +
    "      </div>\n" +
    "      <button class=\"btn btn-success\" type=\"submit\">Login</button>\n" +
    "  </form>\n" +
    "</div>\n" +
    "\n" +
    "</html>");
}]);
