angular.module( 'ngBoilerplate.admin', [
  'ui.router',
  'plusOne',
  'ngBoilerplate.login',
  'ngBoilerplate.employee',
  'ngBoilerplate.books'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'admin_home', {
    url: '/employee',
    views: {
      'main': {
        controller: 'EmployeeCtrl',
        templateUrl: 'admin/admin_home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' },
         resolve: {
             employees: function(employeeInfoService) {
                 return employeeInfoService.getAllEmployees();
             }
         }
})
.state( 'generateReport', {
      url: '/generateReport',
      views: {
        'main': {
          controller: 'ReportCtrl',
          templateUrl: 'admin/reports.tpl.html'
        }
      },
      data:{ pageTitle: 'Generate Report' }
   });
})
.factory('reportService',function($resource,$http,$base64){
    var service={};
    service.generateReport=function(fileType,fileName){
            return $http.post('/library/admins/report', "fileType="+fileType+"&fileName="+fileName, {
                        headers: {'Content-Type':'application/x-www-form-urlencoded'}
                    }).$promise;
        };
    return service;
})
.controller('ReportCtrl',function($scope, $state,sessionService,reportService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.generateReport=function(fileName,fileType){
        reportService.generateReport(fileType.fileType,fileName.fileName);
    };
})
.controller('EmployeeCtrl',function($scope, $state,employeeInfoService,sessionService,employees){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.addEmployee=function(firstName,lastName,email,username,password){
             employeeInfoService.addEmployee({
                 firstName: firstName,
                 lastName: lastName,
                 email:email,
                 username:username,
                 password:password
             }).then(function(){
                 $state.go($state.current,{},{reload:true});
             });
         };
    $scope.employees=employees;
});