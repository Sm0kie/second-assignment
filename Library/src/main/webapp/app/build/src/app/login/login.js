angular.module('ngBoilerplate.login', ['ui.router', 'ngResource', 'base64','ngBoilerplate.home','ngBoilerplate.admin'])
.config(function($stateProvider) {
    $stateProvider.state('login', {
        url:'/login',
        views: {
            'main': {
                templateUrl:'login/login.tpl.html',
                controller: 'LoginCtrl'
            }
        },
        data : { pageTitle : "Login" }
    });
})
.factory('sessionService', function($http, $base64) {
    var session = {};

    session.login = function(data) {
        return $http.post('/library/login', "username="+data.username+"&password="+data.password, {
            headers: {'Content-Type':'application/x-www-form-urlencoded'}
        }).then(function(data){
           localStorage.setItem("session", {});
        }, function(data){
        });
    };
    session.logout = function() {
        localStorage.clear();
    };
    session.isLoggedIn = function() {
        return localStorage.getItem("session",{}) !== null;
    };
    return session;
})
.factory('adminService', function($resource) {
    var service = {};
    service.adminExists = function(user, success, failure) {
        var Admin=$resource("/library/admins") ;
        var data =Admin.get({username:user.username, password:user.password}, function() {
        var admins = data.admins;
        if(admins.length!==0) {
            success(user);
        } else {
            failure();
        }
        },
        failure);
    };
    service.getAdminByUsernameAndPassword=function(admin){
             var Admin=$resource("/library/admins") ;
             var id=Admin.get({username:admin.username, password:admin.password}).$promise.then(function(data){
                return data.admins[0];
             });
    };
    return service;
})
.factory('employeeService', function($resource) {
    var service = {};
    service.employeeExists = function(user, success, failure) {
        var Employee=$resource("/library/employees") ;
        var data =Employee.get({username:user.username, password:user.password}, function() {
        var employees = data.employees;
        if(employees.length!==0) {
            success(user);
        } else {
            failure();
        }
        },
        failure);
    };
    service.getEmployeeByUsernameAndPassword=function(employee){
         var Employee=$resource("/library/employees") ;
         var id=Employee.get({username:employee.username, password:employee.password}).$promise.then(function(data){
            return data.employees[0];
         });
    };
    return service;
})
.controller("LoginCtrl", function($scope,$resource, sessionService,adminService, employeeService,$state) {

    $scope.login = function() {

        employeeService.employeeExists($scope.user, function(user) {
            sessionService.login(user).then(function(){
                    $state.go("home");
            });
            },
        function() {
            adminService.adminExists($scope.user,function(user){
                        sessionService.login(user).then(function(){
                                            $state.go("admin_home");
                                    });
                                    },
                                            function() {
                                                alert("Error logging in user");
                    });
        });
    };
});