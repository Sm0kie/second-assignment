angular.module( 'ngBoilerplate.home', [
  'ui.router',
  'ngBoilerplate.login',
  'ngBoilerplate.books',
  'ngResource'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'home', {
    url: '/home/books',
    views: {
      'main': {
        controller: 'HomeCtrl',
        templateUrl: 'home/home.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' },
    resolve:{
       books: function(bookService){
          return bookService.getAllBooks();
       }
    }
})
.state('sellBook',{
        url:'/home/books/book?bookId',
        views:{
            'main':{
                templateUrl:'home/books.tpl.html',
                controller:'BookCtrl'
            }
        },
        data:{pageTitle:'Sell Book'},
        resolve:{
            book: function(bookService,$stateParams){
                return bookService.findBookById($stateParams.bookId);
            }
        }
});
})
.factory('bookService',function($resource){
    var service={};
    service.getAllBooks=function(){
        var Book=$resource("/library/books");
        return Book.get().$promise.then(function(data){
            return data.books;
        });
    };
    service.findBookById=function(bookId){
        var Book=$resource("/library/books/:paramBookId");
        return Book.get({paramBookId:bookId}).$promise;
    };
    service.sellBooks=function(soldBooks){
        var Book=$resource("/library/books/sell");
        return Book.save(soldBooks).$promise;
    };
     service.removeBook=function(bookId){
        var Book=$resource("/library/books/:paramBookId");
        return Book.remove({paramBookId:bookId}).$promise;
     };
     service.updateBookInfo=function(bookId,updatedBook){
        var Book=$resource("/library/books/:paramBookId");
        return Book.save({paramBookId:bookId},updatedBook).$promise;
     };
     service.addBook=function(savedBook){
        var Book=$resource("/library/books/addBook");
        return Book.save(savedBook).$promise;
     };
    return service;
})
.controller('BookCtrl',function($scope, $state,bookService,sessionService,book){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
    $scope.book=book;
    $scope.sellBooks=function(quantity){
             bookService.sellBooks({
                 "bookId": book.rid,
                 "quantity": $scope.quantity
             }).then(function(){
                 $state.go("home");
             });
         };

})
.controller( 'HomeCtrl', function HomeController( $state,books,$scope, sessionService,bookService,$resource) {
    $scope.isLoggedIn = sessionService.isLoggedIn;
       $scope.logout = sessionService.logout;
       if(!sessionService.isLoggedIn()){
           sessionService.logout();
           $state.go("login");
       }
    $scope.books=books;
});

