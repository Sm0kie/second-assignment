angular.module( 'ngBoilerplate.books', [
  'ui.router',
  'plusOne',
  'base64',
  'ngBoilerplate.login',
  'ngBoilerplate.home'
])
.config(function config( $stateProvider ) {
  $stateProvider.state( 'searchBooks', {
    url: '/library',
    views: {
      'main': {
        controller: 'BookInfoCtrl',
        templateUrl: 'books/books.tpl.html'
      }
    },
    data:{ pageTitle: 'Books' },
    resolve:{
        books: function(bookService){
                  return bookService.getAllBooks();
               }
    }
    })
    .state( 'manageBook', {
        url: '/library/book?bookId',
        views: {
          'main': {
            controller: 'BookManageCtrl',
            templateUrl: 'books/book.tpl.html'
          }
        },
        data:{ pageTitle: 'Book' },
        resolve:{
            book: function(bookService,$stateParams){
                      return bookService.findBookById($stateParams.bookId);
                   }
        }
        })
    .state('updateBook',{
         url:'/library/update/book?bookId',
         views:{
            'main':{
                controller: 'UpdateBookCtrl',
                templateUrl:'books/update_books.tpl.html'
            }
         },
         data:{ pageTitle: 'Update Book Info'},
         resolve:{
            book: function(bookService,$stateParams){
                 return bookService.findBookById($stateParams.bookId);
             }
        }
    });
})
 .controller('BookInfoCtrl',function($scope,$state,bookService,books,sessionService){
      $scope.isLoggedIn = sessionService.isLoggedIn;
      $scope.logout = sessionService.logout;
     if(!sessionService.isLoggedIn()){
             sessionService.logout();
             $state.go("login");
         }
      $scope.books=books;
        $scope.addBook=function(title,author,genre,quantity,price){
            bookService.addBook({
                title: title,
                author: author,
                genre:genre,
                quantity:quantity,
                price:price
            }).then(function(){
                   $state.go($state.current,{},{reload:true});
             });
         };
   })
 .controller('UpdateBookCtrl',function($scope,$state,bookService,book,sessionService){
      $scope.isLoggedIn = sessionService.isLoggedIn;
      $scope.logout = sessionService.logout;
      if(!sessionService.isLoggedIn()){
              sessionService.logout();
              $state.go("login");
          }
      $scope.book=book;

      $scope.updateBook=function(title,author,genre,quantity,price){
         bookService.updateBookInfo(book.rid,{
             title: book.title,
             author:book.author,
             genre:book.genre,
             quantity:book.quantity,
             price: book.price
         }).then(function(){
             $state.go("manageBook",{bookId:book.rid},{reload:true});
         });
      };
   })
.controller('BookManageCtrl',function($scope,$state,bookService,book,sessionService){
    $scope.isLoggedIn = sessionService.isLoggedIn;
    $scope.logout = sessionService.logout;
    if(!sessionService.isLoggedIn()){
        sessionService.logout();
        $state.go("login");
    }
     $scope.book=book;
     $scope.removeBook=function(){
             bookService.removeBook(book.rid).then(function(){
                 $state.go("searchBooks");
             });
     };
 });