package core.security;

import core.models.jaxb.EmployeeJaxb;
import core.models.Employee;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * Created by Chris on 10/19/14.
 */
@Component
public class EmployeeDetailServiceImpl implements UserDetailsService {

    private EmployeeJaxb employeeJaxb=new EmployeeJaxb();

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Employee employee = null;
        try {
            employee = employeeJaxb.findEmployeeByUserName(name);
            if(employee == null) {
                throw new UsernameNotFoundException("no user found with " + name);
            }
            return new EmployeeDetails(employee);
        } catch (JAXBException e) {
        } catch (IOException e) {
        }
       return null;
    }
}
