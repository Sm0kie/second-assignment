package core.security;

import core.models.jaxb.AdminJaxb;
import core.models.Admin;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * Created by ilyes on 3/24/2016.
 */
@Component
public class AdminDetailServiceImpl implements UserDetailsService {

    private AdminJaxb adminJaxb=new AdminJaxb();

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Admin admin = null;
        try {
            admin = adminJaxb.findAdminByUserName(name);
            if(admin == null) {
                throw new UsernameNotFoundException("no user found with " + name);
            }
            return new AdminDetails(admin);
        } catch (JAXBException e) {

        } catch (IOException e) {

        }
        return null;
    }
}
