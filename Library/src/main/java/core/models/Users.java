package core.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * Created by ilyes on 4/3/2016.
 */
@XmlRootElement(namespace="core.models")
public class Users {
    @XmlElementWrapper(name="employeeList")
    @XmlElement(name="employee")
    private ArrayList<Employee> employeeList;
    @XmlElementWrapper(name="adminList")
    @XmlElement(name="admin")
    private ArrayList<Admin> adminList;

    public ArrayList<Admin> getAdmins() {
        return adminList;
    }

    public void setAdminList(ArrayList<Admin> adminList) {
        this.adminList = adminList;
    }
    public ArrayList<Employee> getEmployees() {
        return employeeList;
    }

    public void setEmployee(ArrayList<Employee> employeeList) {
        this.employeeList = employeeList;
    }

}
