package core.models.jaxb;

import core.models.Book;
import core.models.Library;
import core.models.SoldBook;
import core.models.SoldBooks;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 4/4/2016.
 */
public class BookJaxb {
    private final String BOOK_XML;
    public BookJaxb(){
        this.BOOK_XML="./books.xml";
    }

    public List<Book> findAllBooks() throws JAXBException, IOException {
        ArrayList<Book> bookList;
        JAXBContext context= JAXBContext.newInstance(Library.class);
        Unmarshaller um=context.createUnmarshaller();
        Library library = (Library) um.unmarshal(new FileReader(BOOK_XML));
        bookList=library.getBooks();
        return bookList;
    }


    public Book findBookById(Long id) throws JAXBException, IOException {
        ArrayList<Book> bookList = new ArrayList<Book>();
        JAXBContext context=JAXBContext.newInstance(Library.class);
        Unmarshaller um=context.createUnmarshaller();
        Library library=(Library) um.unmarshal(new FileReader(BOOK_XML));
        bookList=library.getBooks();
        for(Book b:bookList)
            if(b.getId()==id)
                return b;
        return null;
    }

    public Book sellBook(Long id,Long quantity) throws JAXBException, IOException {
        ArrayList<Book> bookList = new ArrayList<Book>();
        JAXBContext context=JAXBContext.newInstance(Library.class);
        Unmarshaller um=context.createUnmarshaller();
        Library library=(Library) um.unmarshal(new FileReader(BOOK_XML));
        bookList=library.getBooks();
        for(Book b:bookList)
            if(b.getId()==id) {
                int index = bookList.indexOf(b);
                b.setQuantity(b.getQuantity() - quantity);
                bookList.set(index, b);
                library.setBookList(bookList);
                Marshaller m = context.createMarshaller();
                m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                m.marshal(library, new File(BOOK_XML));
                return b;
            }
        return null;
    }

    public Book findBookByTitle(String title) throws JAXBException, IOException {
        ArrayList<Book> bookList;
        JAXBContext context = JAXBContext.newInstance(Library.class);
        Unmarshaller um = context.createUnmarshaller();
        Library library=(Library) um.unmarshal(new FileReader(BOOK_XML));
        bookList=library.getBooks();
        for(Book b:bookList)
            if(b.getTitle().equals(title))
                return b;
        return null;
    }


    public Book addBook(Book data) throws JAXBException, IOException {
        ArrayList<Book> bookList;
        boolean exists=false;
        JAXBContext context=JAXBContext.newInstance(Library.class);
        Unmarshaller um=context.createUnmarshaller();
        Library library=(Library) um.unmarshal(new FileReader(BOOK_XML));
        bookList=library.getBooks();
        Book book=new Book();
        if(bookList.size()!=0){
            for(Book b:bookList)
                if((b.getTitle().equals(data.getTitle())&&b.getAuthor().equals(data.getAuthor())))
                    exists=true;
        }
        if(!exists){
            if(bookList.size()==0)
                book.setId(1l);
            else
                book.setId(bookList.get(bookList.size()-1).getId()+1);
            book.setAuthor(data.getAuthor());
            book.setPrice(data.getPrice());
            book.setQuantity(data.getQuantity());
            book.setGenre(data.getGenre());
            book.setTitle(data.getTitle());
            bookList.add(book);
            Marshaller m=context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
            library.setBookList(bookList);
            m.marshal(library,new File(BOOK_XML));
        }
        return book;
    }


    public Book updateBook(Long bookId, Book data) throws JAXBException, IOException {
        ArrayList<Book> bookList;
        JAXBContext context = JAXBContext.newInstance(Library.class);
        Unmarshaller um=context.createUnmarshaller();
        Library library = (Library) um.unmarshal(new FileReader(BOOK_XML));
        bookList=library.getBooks();
        Book book=new Book();
        for(Book b:bookList){
            if(b.getId()==bookId){
                int id=bookList.indexOf(b);
                book.setTitle(data.getTitle());
                book.setGenre(data.getGenre());
                book.setQuantity(data.getQuantity());
                book.setPrice(data.getPrice());
                book.setAuthor(data.getAuthor());
                book.setId(bookId);
                bookList.set(id,book);
            }
        }
        library.setBookList(bookList);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        m.marshal(library,new File(BOOK_XML));
        return book;
    }


    public Book removeBook(Long id) throws JAXBException, IOException {
        ArrayList<Book> bookList;
        JAXBContext context=JAXBContext.newInstance(Library.class);
        Unmarshaller um=context.createUnmarshaller();
        Library library = (Library) um.unmarshal(new FileReader(BOOK_XML));
        bookList=library.getBooks();
        Book book=new Book();
        for(Book b:bookList)
            if(b.getId()==id)
                book=b;
        if(book.getId()!=null);
            bookList.remove(book);
        library.setBookList(bookList);
        Marshaller m=context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        m.marshal(library,new File(BOOK_XML));
        return book;
    }

    public void addBook(SoldBook data) throws JAXBException,IOException{
        ArrayList<SoldBook> bookList=new ArrayList<SoldBook>();
        JAXBContext context=JAXBContext.newInstance(SoldBooks.class);
        Unmarshaller um=context.createUnmarshaller();
        SoldBooks soldBooks=(SoldBooks) um.unmarshal(new FileReader("./sold_books.xml"));
        bookList=soldBooks.getSoldList();
        bookList.add(data);
        Marshaller m=context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        soldBooks.setSoldBookList(bookList);
        m.marshal(soldBooks,new File("./sold_books.xml"));
    }
}
