package core.models.jaxb;

import core.models.Admin;
import core.models.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 4/3/2016.
 */
public class AdminJaxb {
    private final String ADMIN_XML;

    public AdminJaxb() {
        ADMIN_XML = "./users.xml";
    }


    public List<Admin> findAllAdmins() throws JAXBException, IOException {
        ArrayList<Admin> adminList;
        JAXBContext context = JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users = (Users) um.unmarshal(new FileReader(ADMIN_XML));
        adminList=users.getAdmins();
        return adminList;

    }


    public Admin findAdminByUserName(String username) throws JAXBException, IOException {
        ArrayList<Admin> adminList;
        JAXBContext context=JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users = (Users) um.unmarshal(new FileReader(ADMIN_XML));
        adminList=users.getAdmins();
        for(Admin ad:adminList)
            if(ad.getUsername().equals(username))
                return ad;
        return null;
    }


    public Admin findAdminById(Long id) throws JAXBException, IOException {
        ArrayList<Admin> adminList=new ArrayList<Admin>();
        JAXBContext context = JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users = (Users) um.unmarshal(new FileReader(ADMIN_XML));
        adminList=users.getAdmins();
        for(Admin ad:adminList)
            if(ad.getId()==id)
                return ad;
        return null;
    }
}
