package core.models.jaxb;

import core.models.Employee;
import core.models.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 4/3/2016.
 */
public class EmployeeJaxb {
    private final String EMPLOYEE_XML;

    public EmployeeJaxb(){
        this.EMPLOYEE_XML="./users.xml";
    }


    public List<Employee> findAllEmployees() throws JAXBException, IOException {
        ArrayList<Employee> employeeList;
        JAXBContext context = JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users =(Users) um.unmarshal(new FileReader(EMPLOYEE_XML));
        employeeList= users.getEmployees();
        return employeeList;
    }


    public Employee findEmployeeById(Long id) throws JAXBException, IOException {
        ArrayList<Employee> employeeList;
        JAXBContext context = JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users =(Users) um.unmarshal(new FileReader(EMPLOYEE_XML));
        employeeList= users.getEmployees();
        for(Employee em:employeeList){
            if(em.getId()==id)
                return em;
        }
        return null;
    }


    public Employee findEmployeeByUserName(String username) throws JAXBException, IOException {
        ArrayList<Employee> employeeList;
        JAXBContext context = JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users =(Users) um.unmarshal(new FileReader(EMPLOYEE_XML));
        employeeList=users.getEmployees();
        for(Employee em:employeeList){
            if(em.getUsername().equals(username))
                return em;
        }
        return null;
    }


    public Employee createEmployee(Employee data) throws JAXBException, IOException {
        ArrayList<Employee> employeeList;
        boolean exists=false;
        JAXBContext context = JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users =(Users) um.unmarshal(new FileReader(EMPLOYEE_XML));
        employeeList= users.getEmployees();
        Employee employee=new Employee();
        if(employeeList.size()!=0) {

            for(Employee em:employeeList){
                if(em.getUsername().equals(data.getUsername()))
                    exists=true;
            }
        }
        if(!exists){
            if(employeeList.size()==0)
                employee.setId(1l);
            else
                employee.setId(employeeList.get(employeeList.size()-1).getId()+1);
            employee.setEmail(data.getEmail());
            employee.setFirstName(data.getFirstName());
            employee.setLastName(data.getLastName());
            employee.setUsername(data.getUsername());
            employee.setPassword(data.getPassword());
            employeeList.add(employee);
            users.setEmployee(employeeList);
            Marshaller m=context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
            m.marshal(users,new File(EMPLOYEE_XML));
        }



        return employee;
    }


    public Employee updateEmployee(Long employeeId, Employee data) throws JAXBException, IOException {
        ArrayList<Employee> employeeList;
        JAXBContext context = JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users =(Users) um.unmarshal(new FileReader(EMPLOYEE_XML));
        employeeList= users.getEmployees();
        Employee employee=new Employee();
        for(Employee em:employeeList){
            if(em.getId()==employeeId) {
                int id=employeeList.indexOf(em);
                employee.setPassword(data.getPassword());
                employee.setUsername(em.getUsername());
                employee.setEmail(data.getEmail());
                employee.setFirstName(data.getFirstName());
                employee.setLastName(data.getLastName());
                employee.setId(employeeId);
                employeeList.set(id,employee);
            }
        }
        users.setEmployee(employeeList);
        Marshaller m=context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        m.marshal(users,new File(EMPLOYEE_XML));
        return employee;
    }


    public Employee removeEmployee(Long id) throws JAXBException, IOException {
        ArrayList<Employee> employeeList;
        JAXBContext context = JAXBContext.newInstance(Users.class);
        Unmarshaller um=context.createUnmarshaller();
        Users users =(Users) um.unmarshal(new FileReader(EMPLOYEE_XML));
        employeeList= users.getEmployees();
        Employee employee=new Employee();
        for(Employee em:employeeList){
            if(em.getId()==id) {
                employee=em;

            }
        }
        if(employee.getId()!=null)
            employeeList.remove(employee);
        users.setEmployee(employeeList);
        Marshaller m=context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        m.marshal(users,new File(EMPLOYEE_XML));
        return employee;
    }
}
