package core.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * Created by ilyes on 4/4/2016.
 */
@XmlRootElement(namespace="core.models")
public class Library {
    @XmlElementWrapper(name="bookList")
    @XmlElement(name="book")
    private ArrayList<Book> bookList;

    public ArrayList<Book> getBooks() {
        return bookList;
    }

    public void setBookList(ArrayList<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public String toString() {
        String str=new String();
        for(Book b:getBooks()){
            str+="Title: "+b.getTitle()+" Author: "+b.getAuthor()+System.lineSeparator();
        }
        return str;
    }

}
