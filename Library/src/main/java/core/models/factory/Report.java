package core.models.factory;

import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * Created by ilyes on 4/8/2016.
 */
public interface Report {
    void generateReport(String filename) throws JAXBException,IOException;
}
