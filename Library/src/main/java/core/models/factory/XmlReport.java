package core.models.factory;

import core.models.Book;
import core.models.Library;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ilyes on 4/8/2016.
 */
public class XmlReport implements Report {
    @Override
    public void generateReport(String filename) throws JAXBException,IOException{
        ArrayList<Book> bookList;
        JAXBContext context= JAXBContext.newInstance(Library.class);
        Unmarshaller um=context.createUnmarshaller();
        Library library = (Library) um.unmarshal(new FileReader("./books.xml"));
        bookList=library.getBooks();
        ArrayList<Book> outOfStock=new ArrayList<Book>();
        for(Book b:bookList){
            if(b.getQuantity()==0)
                outOfStock.add(b);
        }
        String file=new String(filename+".xml");
        library.setBookList(outOfStock);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(library, new File(file));
    }
}
