package core.models.factory;

/**
 * Created by ilyes on 4/8/2016.
 */
public class ReportFactory {
    public Report getInstance(String identifier){
        switch(identifier){
            case "XML":return new XmlReport();
            case "TXT":return new TxtReport();
        }
        return null;
    }
}
