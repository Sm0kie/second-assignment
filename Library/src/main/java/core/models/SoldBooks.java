package core.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

/**
 * Created by ilyes on 4/8/2016.
 */
@XmlRootElement(namespace="core.models")
public class SoldBooks {
    @XmlElementWrapper(name="bookList")
    @XmlElement(name="soldBook")
    private ArrayList<SoldBook> soldBookList;

    public ArrayList<SoldBook> getSoldList() {
        return soldBookList;
    }

    public void setSoldBookList(ArrayList<SoldBook> soldBookList) {
        this.soldBookList = soldBookList;
    }
}
