package rest.resources.asm;

import core.models.Employee;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.EmployeeController;
import rest.resources.EmployeeResource;

import javax.xml.bind.JAXBException;
import java.io.IOException;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ilyes on 4/3/2016.
 */
public class EmployeeResourceAsm extends ResourceAssemblerSupport<Employee,EmployeeResource> {

    public EmployeeResourceAsm()
    {
        super(EmployeeController.class, EmployeeResource.class);
    }

    @Override
    public EmployeeResource toResource(Employee employee) {
        EmployeeResource res = new EmployeeResource();
        res.setRid(employee.getId());
        res.setFirstName(employee.getFirstName());
        res.setLastName(employee.getLastName());
        res.setUsername(employee.getUsername());
        res.setPassword(employee.getPassword());
        res.setEmail(employee.getEmail());
        try {
            res.add(linkTo(methodOn(EmployeeController.class).findEmployeeById(employee.getId())).withSelfRel());
        } catch (JAXBException e) {
        } catch (IOException e) {
        }

        return res;
    }
}
