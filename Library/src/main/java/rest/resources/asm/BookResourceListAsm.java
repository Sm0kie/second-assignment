package rest.resources.asm;

import core.models.Book;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.BookController;
import rest.resources.BookResource;
import rest.resources.BookResourceList;

import java.util.List;

/**
 * Created by ilyes on 4/4/2016.
 */
public class BookResourceListAsm extends ResourceAssemblerSupport<List<Book>,BookResourceList> {
    public BookResourceListAsm(){
        super(BookController.class,BookResourceList.class);
    }

    @Override
    public BookResourceList toResource(List<Book> bookList){
        List<BookResource> resList = new BookResourceAsm().toResources(bookList);
        BookResourceList finalRes=new BookResourceList();
        finalRes.setBooks(resList);
        return finalRes;
    }
}
