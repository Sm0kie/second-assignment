package rest.resources.asm;

import core.models.Admin;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.AdminController;
import rest.resources.AdminResource;

import javax.xml.bind.JAXBException;
import java.io.IOException;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ilyes on 4/3/2016.
 */
public class AdminResourceAsm extends ResourceAssemblerSupport<Admin,AdminResource> {

    public AdminResourceAsm()
    {
        super(AdminController.class, AdminResource.class);
    }

    @Override
    public AdminResource toResource(Admin admin) {
        AdminResource res = new AdminResource();
        res.setRid(admin.getId());
        res.setFirstName(admin.getFirstName());
        res.setLastName(admin.getLastName());
        res.setUsername(admin.getUsername());
        res.setPassword(admin.getPassword());
        res.setEmail(admin.getEmail());
        try {
            res.add(linkTo(methodOn(AdminController.class).findAdminById(admin.getId())).withSelfRel());
        } catch (JAXBException e) {
        } catch (IOException e) {
        }
        return res;
    }
}
