package rest.resources.asm;

import core.models.Book;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import rest.controller.BookController;
import rest.resources.BookResource;

import javax.xml.bind.JAXBException;
import java.io.IOException;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ilyes on 4/4/2016.
 */
public class BookResourceAsm extends ResourceAssemblerSupport<Book,BookResource> {

    public BookResourceAsm(){
        super(BookController.class,BookResource.class);
    }
    @Override
    public BookResource toResource(Book book) {
        BookResource res= new BookResource();
        res.setRid(book.getId());
        res.setPrice(book.getPrice());
        res.setGenre(book.getGenre());
        res.setTitle(book.getTitle());
        res.setQuantity(book.getQuantity());
        res.setAuthor(book.getAuthor());
        try{
                res.add(linkTo(methodOn(BookController.class).findBookById(book.getId())).withSelfRel());
            } catch (JAXBException e) {
            } catch (IOException e) {
        }
        return res;
    }
}
