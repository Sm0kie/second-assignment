package rest.resources;

import core.models.Book;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ilyes on 4/4/2016.
 */
public class BookResource extends ResourceSupport {
    private Long rid;
    private String title;
    private String genre;
    private Long quantity;
    private double price;
    private String author;

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Book toBook(){
        Book book=new Book();
        book.setId(rid);
        book.setGenre(genre);
        book.setPrice(price);
        book.setQuantity(quantity);
        book.setTitle(title);
        book.setAuthor(author);
        return book;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
