package rest.resources;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ilyes on 4/4/2016.
 */
public class BookResourceList extends ResourceSupport {
    private List<BookResource> books= new ArrayList<BookResource>();

    public List<BookResource> getBooks() {
        return books;
    }

    public void setBooks(List<BookResource> books) {
        this.books = books;
    }
}
