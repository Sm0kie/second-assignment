package rest.controller;

import core.models.jaxb.EmployeeJaxb;
import core.models.Employee;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.EmployeeResource;
import rest.resources.EmployeeResourceList;
import rest.resources.asm.EmployeeResourceAsm;
import rest.resources.asm.EmployeeResourceListAsm;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ilyes on 4/3/2016.
 */
@RestController
@RequestMapping(value="/employees")
public class EmployeeController {
    private EmployeeJaxb employeeJaxb =new EmployeeJaxb();

    @RequestMapping(method= RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<EmployeeResourceList> findAllEmployees(@RequestParam(value="username",required=false) String username,@RequestParam(value="password",required=false) String password)throws JAXBException, IOException{
        List<Employee> empl= null;
        if(username==null){
            empl= employeeJaxb.findAllEmployees();
        }
        else{
            Employee employee= employeeJaxb.findEmployeeByUserName(username);
            empl=new ArrayList<Employee>();
            if(employee!=null){
                if(password!=null){
                    if(employee.getPassword().equals(password)){
                        empl=new ArrayList<Employee>(Arrays.asList(employee));
                    }
                }else{
                    empl=new ArrayList<Employee>(Arrays.asList(employee));
                }
            }
        }
        if(!empl.isEmpty()) {
            EmployeeResourceList res = new EmployeeResourceListAsm().toResource(empl);
            return new ResponseEntity<EmployeeResourceList>(res, HttpStatus.OK);
        }
        return new ResponseEntity<EmployeeResourceList>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="/{employeeId}",method=RequestMethod.GET)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<EmployeeResource> findEmployeeById(@PathVariable Long employeeId)throws JAXBException, IOException{
        Employee employee = employeeJaxb.findEmployeeById(employeeId);
        EmployeeResource res = new EmployeeResourceAsm().toResource(employee);
        return new ResponseEntity<EmployeeResource>(res,HttpStatus.OK);
    }
    @RequestMapping(value="/createEmployee",method=RequestMethod.POST)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<EmployeeResource> createEmployee(@RequestBody Employee sentAccount, UriComponentsBuilder ucBuilder) throws JAXBException, IOException {
        Employee employee = employeeJaxb.createEmployee(sentAccount);
        if(employee.getId()!=null) {
            EmployeeResource res = new EmployeeResourceAsm().toResource(employee);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/employees/{employeeId}").buildAndExpand(res.getId()).toUri());
            return new ResponseEntity<EmployeeResource>(res, HttpStatus.CREATED);
        }
        return new ResponseEntity<EmployeeResource>(HttpStatus.CONFLICT);
    }
    @RequestMapping(value = "/{employeeId}", method = RequestMethod.POST)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<EmployeeResource> updateEmployee(@PathVariable Long employeeId, @RequestBody Employee sentAccount) throws JAXBException, IOException{
        Employee updatedEmployee = employeeJaxb.updateEmployee(employeeId, sentAccount);
        if(updatedEmployee.getId()==null) {
            EmployeeResource res = new EmployeeResourceAsm().toResource(updatedEmployee);
            return new ResponseEntity<EmployeeResource>(res, HttpStatus.OK);
        }
        return new ResponseEntity<EmployeeResource>(HttpStatus.NOT_FOUND);
    }
    @RequestMapping(value="/{employeeId}",method=RequestMethod.DELETE)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<EmployeeResource> removeEmployee(@PathVariable Long employeeId)throws JAXBException, IOException{
        Employee employee = employeeJaxb.removeEmployee(employeeId);
        if(employee!=null) {
            EmployeeResource res = new EmployeeResourceAsm().toResource(employee);
            return new ResponseEntity<EmployeeResource>(res, HttpStatus.OK);
        }
        return new ResponseEntity<EmployeeResource>(HttpStatus.NOT_FOUND);
    }
}