package rest.controller;

import core.models.jaxb.BookJaxb;
import core.models.Book;
import core.models.SellingInfo;
import core.models.SoldBook;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import rest.resources.BookResource;
import rest.resources.BookResourceList;
import rest.resources.asm.BookResourceAsm;
import rest.resources.asm.BookResourceListAsm;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

/**
 * Created by ilyes on 4/4/2016.
 */
@RestController
@RequestMapping(value="/books")
public class BookController {
    private BookJaxb bookJaxb =new BookJaxb();

    @RequestMapping(method=RequestMethod.GET)
    @PreAuthorize("hasRole('USER') OR hasRole('admin')")
    public ResponseEntity<BookResourceList> findAllBooks() throws JAXBException, IOException{
        List<Book> books= bookJaxb.findAllBooks();
        if(!books.isEmpty()){
            BookResourceList res = new BookResourceListAsm().toResource(books);
            return new ResponseEntity<BookResourceList>(res,HttpStatus.OK);
        }
        return new ResponseEntity<BookResourceList>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="/{bookId}",method= RequestMethod.GET)
    @PreAuthorize("hasRole('USER') OR hasRole('admin')")
    public ResponseEntity<BookResource> findBookById(@PathVariable Long bookId) throws JAXBException,IOException {
        Book book= bookJaxb.findBookById(bookId);
        if(book!=null){
            BookResource res=new BookResourceAsm().toResource(book);
            return new ResponseEntity<BookResource>(res, HttpStatus.OK);
        }
        return new ResponseEntity<BookResource>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="/addBook",method=RequestMethod.POST)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<BookResource> addBook(@RequestBody Book sentBook, UriComponentsBuilder ucBuilder) throws JAXBException,IOException{
        Book book = bookJaxb.addBook(sentBook);
        if(book.getId()!=null){
            BookResource res=new BookResourceAsm().toResource(book);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/books/{bookId}").buildAndExpand(res.getRid()).toUri());
            return new ResponseEntity<BookResource>(res,HttpStatus.CREATED);
        }
        return new ResponseEntity<BookResource>(HttpStatus.CONFLICT);
    }

    @RequestMapping(value="/{bookId}",method=RequestMethod.POST)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<BookResource> updateBook(@PathVariable Long bookId, @RequestBody Book sentBook) throws JAXBException,IOException{
        Book updatedBook = bookJaxb.updateBook(bookId,sentBook);
        if(updatedBook.getId()!=null){
            BookResource res = new BookResourceAsm().toResource(updatedBook);
            return new ResponseEntity<BookResource>(res,HttpStatus.OK);
        }
        return new ResponseEntity<BookResource>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="/{bookId}",method=RequestMethod.DELETE)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<BookResource> removeBook(@PathVariable Long bookId) throws JAXBException,IOException{
        Book book = bookJaxb.removeBook(bookId);
        if(book!=null){
            BookResource res = new BookResourceAsm().toResource(book);
            return new ResponseEntity<BookResource>(res,HttpStatus.OK);
        }
        return new ResponseEntity<BookResource>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="/sell",method=RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<BookResource> sellBook(@RequestBody SellingInfo sellingInfo)throws JAXBException,IOException{
        Book book = bookJaxb.findBookById(sellingInfo.getBookId());
        if(book.getQuantity()< sellingInfo.getQuantity()){
            return new ResponseEntity<BookResource>(HttpStatus.NOT_ACCEPTABLE);
        }
        book= bookJaxb.sellBook(sellingInfo.getBookId(), sellingInfo.getQuantity());
        SoldBook sold=new SoldBook();
        sold.setQuantity(sellingInfo.getQuantity());
        sold.setTitle(book.getTitle());
        sold.setAuthor(book.getAuthor());
        sold.setAmount(sold.getQuantity()*book.getPrice());
        bookJaxb.addBook(sold);
        BookResource res = new BookResourceAsm().toResource(book);
        return new ResponseEntity<BookResource>(res,HttpStatus.OK);
    }
}
