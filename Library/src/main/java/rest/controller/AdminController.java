package rest.controller;

import core.models.jaxb.AdminJaxb;
import core.models.Admin;
import core.models.factory.ReportFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rest.resources.AdminResource;
import rest.resources.AdminResourceList;
import rest.resources.asm.AdminResourceAsm;
import rest.resources.asm.AdminResourceListAsm;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ilyes on 4/3/2016.
 */
@RestController
@RequestMapping(value="/admins")
public class AdminController {
    private AdminJaxb adminJaxb =new AdminJaxb();

    @RequestMapping(method= RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<AdminResourceList> findAllAdmins(@RequestParam(value="username",required=false) String username, @RequestParam(value="password",required=false) String password) throws JAXBException, IOException {
        List<Admin> admin=null;
        if(username==null){
            admin= adminJaxb.findAllAdmins();
        }else{
            Admin ad = adminJaxb.findAdminByUserName(username);
            admin=new ArrayList<Admin>();
            if(ad!=null){
                if(password!=null){
                    if(ad.getPassword().equals(password)){
                        admin=new ArrayList<Admin>(Arrays.asList(ad));
                    }
                }else{
                    admin=new ArrayList<Admin>(Arrays.asList(ad));
                }
            }
        }
        if(!admin.isEmpty()) {
            AdminResourceList res = new AdminResourceListAsm().toResource(admin);
            return new ResponseEntity<AdminResourceList>(res, HttpStatus.OK);
        }
        return new ResponseEntity<AdminResourceList>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="/{adminId}",method=RequestMethod.GET)
    @PreAuthorize("permitAll")
    public ResponseEntity<AdminResource> findAdminById(@PathVariable Long adminId) throws JAXBException,IOException{
        Admin admin= adminJaxb.findAdminById(adminId);
        if(admin!=null){
            AdminResource res=new AdminResourceAsm().toResource(admin);
            return new ResponseEntity<AdminResource>(res,HttpStatus.OK);
        }
        return new ResponseEntity<AdminResource>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value="/report",method=RequestMethod.POST)
    @PreAuthorize("hasRole('admin')")
    public ResponseEntity<Void> generateReport(@RequestParam(value="fileType") String fileType,@RequestParam (value="fileName") String fileName)throws JAXBException,IOException{
        new ReportFactory().getInstance(fileType).generateReport(fileName);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
